#!/bin/bash

############################## START UP CONFIG ###################################
make_conky_start()
{
    echo "[Desktop Entry]"
    echo "Encoding=UTF-8"
    echo "Version=0.9.4"
    echo "Type=Application"
    echo "Name=Conky"
    echo "Comment=Conky System Monitor"
    echo "Exec=conky"
    echo "OnlyShowIn=XFCE;"
    echo "StartupNotify=false"
    echo "Terminal=false"
    echo "Hidden=false"
}

make_redshift_start()
{
    echo "[Desktop Entry]"
    echo "Version=1.0"
    echo "Name=Redshift"
    echo "Name[de]=Redshift"
    echo "Name[es]=Redshift"
    echo "Name[fr]=Redshift"
    echo "Name[hu]=Redshift"
    echo "Name[it]=Redshift"
    echo "Name[lt]=Redshift"
    echo "Name[pl]=Redshift"
    echo "Name[pt_BR]=Redshift"
    echo "Name[ru]=Redshift"
    echo "Name[tr]=Redshift"
    echo "Name[zh_CN]=红移"
    echo "GenericName=Color temperature adjustment"
    echo "GenericName[cs]=Přizpůsobení barevné teploty"
    echo "GenericName[da]=Justering af farvetemperatur"
    echo "GenericName[de]=Farbtemperatur Anpassung"
    echo "GenericName[es]=Ajuste de la temperatura de color"
    echo "GenericName[fr]=Réglage de la température de couleur"
    echo "GenericName[hu]=Szín hőmérséklet beállítás"
    echo "GenericName[it]=Regolazione della temperatura del colore"
    echo "GenericName[lt]=Spalvos temperatūros reguliavimas"
    echo "GenericName[nl]=Kleurtemperatuur aanpassing"
    echo "GenericName[pl]=Dostosowanie temperatury barwowej"
    echo "GenericName[ru]=Настройка цветовой температуры"
    echo "GenericName[tr]=Renk sıcaklığı ayarı"
    echo "GenericName[zh_CN]=色温调节"
    echo "Comment=Color temperature adjustment tool"
    echo "Comment[cs]=Nástroj pro přizpůsobení barevné teploty"
    echo "Comment[da]=Justeringsværktøj for farvetemperatur"
    echo "Comment[de]=Farbtemperatur Anpassungswerkzeug"
    echo "Comment[es]=Herramienta para el ajuste de la temperatura de color"
    echo "Comment[fr]=Outil de réglage de la température de couleur"
    echo "Comment[hu]=Színhőmérséklet beállító eszköz"
    echo "Comment[it]=Strumento per la regolazione della temperatura del colore"
    echo "Comment[lt]=Spalvos temperatūros reguliavimo įrankis"
    echo "Comment[nl]=Kleurtemperatuur aanpassingsprogramma"
    echo "Comment[pl]=Narzędzie do dostosowywania temperatury barwowej"
    echo "Comment[ru]=Инструмент регулирования цветовой температуры"
    echo "Comment[tr]=Renk sıcaklığı ayarlama aracı"
    echo "Comment[zh_CN]=色温调节工具"
    echo "Exec=redshift-gtk"
    echo "Icon=redshift"
    echo "Terminal=false"
    echo "Type=Application"
    echo "Categories=Utility;"
    echo "StartupNotify=true"
    echo "Hidden=false"
    echo "X-GNOME-Autostart-enabled=true"
    echo " "
}

#--------------------------------------------------------------------------------#

################################# CONFIG FILES ###################################
#Conky
make_conky_rc()
{
    echo "update_interval 1.5"

    echo "double_buffer yes"
    echo "text_buffer_size 1024"
    echo "override_utf8_locale yes"
    echo "use_xft yes"
    echo "xftalpha 0.4"
    echo "uppercase yes"

    echo "gap_x 0"
    echo "gap_y 0"
    echo "minimum_size 300 1040"
    echo "maximum_width 350"
    echo "own_window yes"
    echo "own_window_type normal"
    echo "own_window_argb_visual yes"
    echo "own_window_argb_value 150"
    echo "own_window_hints undecorated,below,sticky,skip_taskbar,skip_pager"

    echo "alignment top_right"
    echo "draw_graph_borders yes"

    # Defining colors
    echo "default_color FFFFFF"

    # Shades of Gray
    echo "color1 DDDDDD"
    echo "color2 AAAAAA"
    echo "color3 888888"
    # Orange
    echo "color4 EF5A29"
    # Green
    echo "color5 77B753"

    ## System information using conky capabilities

    echo "TEXT"
    echo "\${voffset 20}"
    echo "\${font Ubuntu:size=10,weight:bold}\${color4}SYSTEM \${hr 2}"
    echo "\${offset 15}\${font Ubuntu:size=10,weight:normal}\${color1}\$sysname \$kernel"
    echo "\${offset 15}\${font Ubuntu:size=10,weight:normal}\${color1}\$nodename Uptime: \$uptime"

    # Showing CPU Graph
    echo "\${voffset -40}"
    echo "\${offset 200}\${cpugraph 30,100 666666 666666}\${voffset -25}"
    echo "\${offset 90}\${font Ubuntu:size=10,weight:bold}\${color5}CPU | \${cpu cpu0}%"
    # Showing TOP 10 CPU-consumers
    echo "\${offset 105}\${font Ubuntu:size=10,weight:normal}\${color4}\${top name 1}\${alignr}\${top cpu 1}%"
    echo "\${offset 105}\${font Ubuntu:size=10,weight:normal}\${color1}\${top name 2}\${alignr}\${top cpu 2}%"
    echo "\${offset 105}\${font Ubuntu:size=10,weight:normal}\${color2}\${top name 3}\${alignr}\${top cpu 3}%"
    echo "\${offset 105}\${font Ubuntu:size=10,weight:normal}\${color3}\${top name 4}\${alignr}\${top cpu 4}%"
    echo "\${offset 105}\${font Ubuntu:size=10,weight:normal}\${color3}\${top name 5}\${alignr}\${top cpu 5}%"
    echo "\${offset 105}\${font Ubuntu:size=10,weight:normal}\${color3}\${top name 6}\${alignr}\${top cpu 6}%"
    echo "\${offset 105}\${font Ubuntu:size=10,weight:normal}\${color3}\${top name 7}\${alignr}\${top cpu 7}%"
    echo "\${offset 105}\${font Ubuntu:size=10,weight:normal}\${color3}\${top name 8}\${alignr}\${top cpu 8}%"
    echo "\${offset 105}\${font Ubuntu:size=10,weight:normal}\${color3}\${top name 9}\${alignr}\${top cpu 9}%"
    echo "\${offset 105}\${font Ubuntu:size=10,weight:normal}\${color3}\${top name 10}\${alignr}\${top cpu 10}%"

    #Showing memory part with TOP 10
    echo "\${voffset 5}"
    echo "\${offset 90}\${font Ubuntu:size=10,weight:bold}\${color5}MEM | \${mem}"
    echo "\${offset 105}\${font Ubuntu:size=10,weight:normal}\${color4}\${top_mem name 1}\${alignr}\${top_mem mem 1}%"
    echo "\${offset 105}\${font Ubuntu:size=10,weight:normal}\${color1}\${top_mem name 2}\${alignr}\${top_mem mem 2}%"
    echo "\${offset 105}\${font Ubuntu:size=10,weight:normal}\${color2}\${top_mem name 3}\${alignr}\${top_mem mem 3}%"
    echo "\${offset 105}\${font Ubuntu:size=10,weight:normal}\${color3}\${top_mem name 4}\${alignr}\${top_mem mem 4}%"
    echo "\${offset 105}\${font Ubuntu:size=10,weight:normal}\${color3}\${top_mem name 5}\${alignr}\${top_mem mem 5}%"
    echo "\${offset 105}\${font Ubuntu:size=10,weight:normal}\${color3}\${top_mem name 6}\${alignr}\${top_mem mem 6}%"
    echo "\${offset 105}\${font Ubuntu:size=10,weight:normal}\${color3}\${top_mem name 7}\${alignr}\${top_mem mem 7}%"
    echo "\${offset 105}\${font Ubuntu:size=10,weight:normal}\${color3}\${top_mem name 8}\${alignr}\${top_mem mem 8}%"
    echo "\${offset 105}\${font Ubuntu:size=10,weight:normal}\${color3}\${top_mem name 9}\${alignr}\${top_mem mem 9}%"
    echo "\${offset 105}\${font Ubuntu:size=10,weight:normal}\${color3}\${top_mem name 10}\${alignr}\${top_mem mem 10}%"

    # Showing disk partitions: root, home and Data
    echo "\${offset 200}\${diskiograph 30,100 666666 666666}\${voffset -25}"
    echo "\${offset 90}\${font Ubuntu:size=10,weight:bold}\${color5}DISKS"
    echo "\${offset 105}\${font Ubuntu:size=9,weight:bold}\${color1}Free: \${font Ubuntu:size=9,weight:normal}\${fs_used /}/\${fs_free /}"

    #echo "\${voffset 10}"
    #echo "\${offset 90}\${font Ubuntu:size=10,weight:bold}\${color5}GPU | \${nvidia temp} C "
    #echo "\${offset 105}\${font Ubuntu:size=9,weight:bold}\${color1}"
    #echo "\${offset 105}GPU FREQ \${nvidia gpufreq} Mhz "
    #echo "\${offset 105}MEM FREQ \${nvidia memfreq} Mhz"

    # Network data
    echo "\${voffset 10}"
    echo "\${offset 90}\${font Ubuntu:size=10,weight:bold}\${color5}NETWORK"
    echo "\${offset 15}\${color1}\${font Ubuntu:size=9,weight:bold}Up: \${alignr}\${font Ubuntu:size=9,weight:normal}\$color2\${upspeed wlp3s0} / \${totalup}"
    echo "\${offset 15}\${upspeedgraph wlp3s0 20,285 4B1B0C FF5C2B 100 -l}"
    echo "\${offset 15}\${color1}\${font Ubuntu:size=9,weight:bold}Down: \${alignr}\${font Ubuntu:size=9,weight:normal}\$color2\${downspeed wlp3s0} / \${totaldown}"
    echo "\${offset 15}\${downspeedgraph wlp3s0 20,285 324D23 77B753 100 -l}"
    echo "\${offset 15}\${downspeedgraph wlp3s0 20,285 324D23 77B753 100 -l}"
    echo "\${font Ubuntu:size=8,weight:bold}\${color5}INTERNAL IP:\${addr wlp3s0}"
    echo "EXTERNAL\${execi 30 ps -a | grep openvpn -w | cut -c 25-31} IP:\${execi 30 curl  ipinfo.io/ip }"

    echo "\${color4}\${hr 2}"

}

#--------------------------------------------------------------------------------#

