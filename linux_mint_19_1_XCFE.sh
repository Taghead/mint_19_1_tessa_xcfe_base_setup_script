#!/bin/bash
#TODO
#BASE INSTALL OR INTERACTIVE INTERFACE CHOICE
# MAKE INTERACTIVE INTERFACE
# ABLE TO SELECT INSTALLS
# ABLE TO SELECT REMOVES
# USER INPUT BASED INSTALLS eg. Install more? SugerApp
# USER INPUT BASED REMOVE

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
cd "$DIR"
. ./library.lib
. ./configs.lib

bold=$(tput bold)  
reset=$(tput sgr0)
backgroundG="\e[100m"
backgroundDG="\e[48;5;236m"
backgroundR="\e[48;5;124m"
backgroundB="\e[40m"
backgroundGreen="\e[130m"

################################# REPOSITORY #######################################
#Virtualbox
echo -e "$backgroundG $bold \n\t==========GETTING REPOSITORY\t\t $1 $reset$backgroundG"
wget -q https://www.virtualbox.org/download/oracle_vbox_2016.asc -O- | sudo apt-key add -
echo "deb [arch=amd64] http://download.virtualbox.org/virtualbox/debian bionic contrib" | sudo tee /etc/apt/sources.list.d/virtualbox.list
#----------------------------------------------------------------------------------#

################################### INSTALL #########################################
echo -e "$backgroundG $bold \n\t==========INSTALLING PACKAGES\t\t $1 $reset$backgroundG"

sudo apt-get update >> ./.dump
sudo rm ./.dump

apt_get "conky-all" "conky-all"
make_conky_rc >> conky_theme.tmp
sudo mv conky_theme.tmp ~/.conkyrc

apt_get "redshift" "redshift"
apt_get "tlp" "tlp" 
apt_get "tlp-rdw" "tlp-rdw"
apt_get "mint-meta-codecs" "mint-meta-codecs"
apt_get "openjdk-8-jdk" "openjdk-8-jdk:amd64"
apt_get "openjdk-8-jre" "openjdk-8-jre:amd64"
apt_get "screenfetch" "screenfetch"
apt_get "wget" "wget"
apt_get "git" "git"
apt_get "code" "code"
apt_get "virtualbox-6.0" "virtualbox-6.0"
#----------------------------------------------------------------------------------#

################################# INSTALL DEBS #####################################
echo -e "$backgroundG $bold \n\t==========INSTALLING DEB PACKAGES\t\t $1 $reset$backgroundG"

apt_deb "megasync-xUbuntu_18.04_amd64" "megasync"

if [ -d ~/MEGA/Programs/Linux/PacketTracer7.2.1 ]; then
    sudo apt deb ~/MEGA/Programs/Linux/PacketTracer7.2.1/libpng.deb
    bash ~/MEGA/Programs/Linux/PacketTracer7.2.1/install
fi

if [ -d ~/MEGA/Programs/Linux ]; then
    apt_deb "~/MEGA/Programs/Linux/discord-0.0.8.deb" "discord"
    apt_deb "~/MEGA/Programs/Linux/code_1.31.0-1549443364_amd64.deb" "code"
fi
#----------------------------------------------------------------------------------#

############################## START UP CONFIGS ####################################
sudo mkdir -p  ~/.config/autostart

make_redshift_start >> redshift-gtk.desktop.tmp
sudo mv redshift-gtk.desktop.tmp ~/.config/autostart/redshift-gtk.desktop

make_conky_start >> conky_theme_start.tmp
sudo mv conky_theme_start.tmp ~/.config/autostart/Conky.desktop
#----------------------------------------------------------------------------------#

################################### REMOVE #########################################
echo -e "$backgroundDG $bold \n\t==========REMOVING PACKAGES\t\t $1 $reset$backgroundDG"
apt_remove "thunderbird"
apt_remove "hexchat"
apt_remove "hexchat-common"

apt_remove "transmission-gtk"
apt_remove "transmission-common"

apt_remove "xplayer"
apt_remove "gir1.2-xplayer-1.0"
apt_remove "gir1.2-xplayer-plparser-1.0"
apt_remove "libxplayer-plparser18"
apt_remove "libxplayer0"
apt_remove "xplayer-common"

apt_remove "pix"
apt_remove "pix-data"

apt_remove "onboard"
apt_remove "onboard-common"

apt_remove "tomboy"
#----------------------------------------------------------------------------------#

################################### UPDATE #########################################
echo -e "$backgroundR $bold \n\t==========UPDATING\t\t $1 $reset$backgroundR"
sudo apt-get update -y
echo Upgrading
sudo apt-get upgrade -y
#----------------------------------------------------------------------------------#

################################### PERSONALIZE ####################################
apt_get "arc-theme"
set_background
echo Changing Theme
xfconf-query -c xsettings -p /Net/ThemeName -s "Arc-Dark"
xfconf-query -c xfwm4 -p /general/theme -s "Arc-Darker"
#----------------------------------------------------------------------------------#

################################### MISC ###########################################
#Display System Details
screenfetch
inxi -Fxz

#Bus Error Workaround
echo ===========BUS ERROR WORKAROUND================
echo Use sudo nano /etc/default/grub
echo Look for GRUB_CMDLINE_LINUX_DEFAULT="quiet splash"
echo Change to GRUB_CMDLINE_LINUX_DEFAULT="quiet splash pci=nomsi"
echo CTRL+O then ENTER then CTRL+X
echo Finally use sudo update-grub
echo ----------------------------------------------

#Suspend Black Screen Workaround
echo =====SUSPEND BLACK SCREEN ERROR WORKAROUND=====
echo "Go to device manager and set to nvidia"
echo "Open NVidia X Server Settings"
echo "Set PRIME Profiles to Intel(Power saving mode)"
echo ----------------------------------------------
#----------------------------------------------------------------------------------#


