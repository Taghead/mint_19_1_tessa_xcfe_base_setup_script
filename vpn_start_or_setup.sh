#For usage with NORDVPN
#Might need a check for if installed 
#Currently uses conky to check if VPN is running

############# Checks for required folder and assumes not installed ################
if [ ! -d "/etc/openvpn" ]; then
    sudo apt-get install openvpn -y
    cd /etc/openvpn
    sudo wget https://downloads.nordcdn.com/configs/archives/servers/ovpn.zip
    sudo apt-get install ca-certificates -y
    sudo apt-get install unzip -y
    sudo unzip ovpn.zip
    sudo rm ovpn.zip
fi
#--------------------------------------------------------------------------------#

################################# Select Server ##################################
#Intended method
# 1. Ask TCP or UDP
# 2. Save selection as $PROTOCOL
# 3. Use ls to display files
# 4. Arrange file names into array
# 5. Display file names
# 6. Save selected file name as $SELECTED_SERVER
# 7. Use 'sudo openvpn /etc/openvpn/ovpn_$PROTOCOL/$SELECTED_SERVER'

ls /etc/openvpn/ovpn_tcp/ | tr "\n" " "

#--------------------------------------------------------------------------------#

################################# Starts VPN #####################################
sudo openvpn /etc/openvpn/ovpn_tcp/au180.nordvpn.com.tcp.ovpn
#--------------------------------------------------------------------------------#